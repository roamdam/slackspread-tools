import unittest
from unittest.mock import patch
from pyux.logging import init_logger
from os import path
from os import remove
from pathlib import Path

from easyspread import NetworkWait

THIS_FOLDER = Path(path.dirname(path.abspath(__file__)))


class TestNetworkwait(unittest.TestCase):
    @patch("easyspread.NetworkWait.stubborn_ping")
    def test_init(self, mping):
        response = NetworkWait(delay=2, hostname="hello")
        self.assertFalse(response.connected)
        mping.assert_called_once_with(delay=2, hostname="hello")

    @patch("socket.create_connection")
    @patch("socket.gethostbyname")
    def test_ping_true(self, mhost, mconnect):
        hostname = "hello"
        response = NetworkWait.ping(hostname=hostname)

        mhost.assert_called_once_with(hostname)
        mconnect.assert_called_once_with((mhost(), 80), 2)

        self.assertTrue(response)

    @patch("socket.gethostbyname")
    def test_ping_false(self, mhost):
        mhost.side_effect = Exception

        response = NetworkWait.ping(hostname="hello")

        self.assertFalse(response)

    @patch("easyspread.NetworkWait.ping")
    def test_stubborn_ping(self, mping):
        mping.return_value = True

        wait = NetworkWait(delay=2, hostname="hello")

        self.assertTrue(wait.connected)

    @patch("sys.stdout.write")
    @patch("easyspread.NetworkWait.ping")
    def test_stubborn_logger(self, mping, mstdout):
        _ = init_logger(
            folder=THIS_FOLDER, filename="delete", run_name="test", time_format="none"
        )
        mping.return_value = True

        NetworkWait(delay=2, hostname="hello")

        mstdout.assert_called()
        remove(THIS_FOLDER / "delete_test_none.log")

    @patch("easyspread.Timer")
    @patch("easyspread.NetworkWait.ping")
    def test_stubborn_loop(self, mping, mtimer):
        mping.return_value = False
        mtimer.side_effect = TimeoutError

        with self.assertRaises(TimeoutError):
            NetworkWait(delay=1, hostname="hello")
        mtimer.assert_called_once_with(
            delay=1, message="No connection to internet, wait then try again"
        )
