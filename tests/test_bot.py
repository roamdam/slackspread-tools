import unittest
from unittest.mock import patch

from slackbot import SlackBot


class TestInit(unittest.TestCase):
    @patch("slack.WebClient")
    def test_init(self, mclient):
        mclient().auth_test().data = {"user_id": "an id"}
        bot = SlackBot(token="A token")
        self.assertEqual("an id", bot.id)

    @patch("slack.WebClient")
    def test_repr(self, _):
        bot = SlackBot(token="A token")
        bot.id = "bot id"
        target = "SlackBot(id = bot id)"

        response = repr(bot)

        self.assertEqual(target, response)


class TestMethods(unittest.TestCase):
    @patch("slack.WebClient")
    def test_user_list(self, mclient):
        mclient().users_list().data = {
            "members": [
                {
                    "real_name": "Logan",
                    "id": "Wolverine",
                    "profile": {"email": "w@x-men.com"},
                    "deleted": False,
                },
                {
                    "real_name": "Jean",
                    "id": "Jean Grey",
                    "profile": {"email": "jg@x-men.com"},
                    "deleted": True,
                },
            ]
        }
        target_list = [{"name": "Logan", "id": "Wolverine", "email": "w@x-men.com"}]
        users = SlackBot("A token").users_list()
        self.assertEqual(target_list, users)

    @patch("slack.WebClient")
    def test_users_noemail(self, mclient):
        mclient().users_list().data = {
            "members": [
                {
                    "real_name": "Logan",
                    "id": "Wolverine",
                    "profile": {"bot": "False"},
                    "deleted": False,
                }
            ]
        }
        target_list = []
        users = SlackBot("A token").users_list()
        self.assertEqual(target_list, users)

    def test_parse_nomention(self):
        message = "Logan is scary"
        target = (None, "Logan is scary")

        response = SlackBot.parse_mention(
            message_text=message, regex=SlackBot.mention_regex
        )

        self.assertEqual(response, target)

    def test_parse_mention(self):
        message = "<@UXAERT> is scary"
        target = ("UXAERT", "is scary")

        response = SlackBot.parse_mention(
            message_text=message, regex=SlackBot.mention_regex
        )

        self.assertEqual(response, target)

    @patch("slack.WebClient")
    def test_send_message(self, mclient):
        SlackBot(token="A token").send_message(channel="a channel", message="a message")
        mclient().chat_postMessage.assert_called_once_with(
            channel="a channel", text="a message"
        )


class TestClosingTime(unittest.TestCase):
    def test_read_time_ok(self):
        target = (20, 45)
        input_time = "20:45"
        response = SlackBot._read_time(x=input_time)
        self.assertEqual(response, target)

    def test_read_time_onedigit(self):
        input_time = "9:45"
        with self.assertRaises(AssertionError):
            SlackBot._read_time(x=input_time)

    def test_read_time_nohour(self):
        input_time = "25:12"
        with self.assertRaises(AssertionError):
            SlackBot._read_time(x=input_time)

    def test_read_time_nominute(self):
        input_time = "23:61"
        with self.assertRaises(AssertionError):
            SlackBot._read_time(x=input_time)

    @patch("slack.RTMClient")
    @patch("slackbot.SlackBot._read_time")
    def test_closing(self, mtime, mrtm):
        mtime.return_value = (8, 45)
        SlackBot.closing_time(closing_hour="08:45", rtm_client=mrtm())
        mrtm().stop.assert_called_once_with()

    def test_not_closing(self):
        self.assertIsNone(SlackBot.closing_time(closing_hour="23:59", rtm_client=None))

    @patch("slackbot.SlackBot._read_time")
    def test_rtm_attribute_error(self, mtime):
        mtime.return_value = (3, 24)
        self.assertIsNone(SlackBot.closing_time(closing_hour="03:24", rtm_client=None))
