import unittest
from unittest.mock import patch
from unittest.mock import mock_open
from os import path
from pathlib import Path

from easyspread import Gspread

THIS_FOLDER = Path(path.dirname(path.abspath(__file__)))


class TestAuthorize(unittest.TestCase):
    @patch("os.environ.get")
    @patch("builtins.open", new_callable=mock_open, read_data="{}")
    def test_get_credentials(self, _, mock_envget):
        mock_envget.return_value = "a key"
        target_credits = {
            "project_id": "a key",
            "private_key_id": "a key",
            "private_key": "a key",
            "client_email": "a key",
            "client_id": "a key",
            "client_x509_cert_url": "a key",
        }

        creds = Gspread.get_credentials(prefix="prefix", file="tata")
        self.assertEqual(creds, target_credits)

    def test_init(self):
        spread = Gspread(
            name="aspread",
            environ_prefix="OFFICES",
            credentials=THIS_FOLDER / ".credentials.json",
        )
        self.assertEqual(spread.name, "aspread")
        self.assertEqual(spread.sheets, dict())


class TestMethods(unittest.TestCase):
    def setUp(self) -> None:
        self.spread = Gspread(
            name="A spread",
            environ_prefix="OFFICES",
            credentials=THIS_FOLDER / ".credentials.json",
        )

    def test_repr(self):
        self.assertEqual(repr(self.spread), "Gspread(name = A spread)")

    @patch("gspread.authorize")
    def test_open_worksheet(self, mspread):
        mspread().open().worksheet.return_value = "A worksheet"
        self.spread.open_worksheet(title="My worksheet")

        self.assertTrue("My worksheet" in self.spread.sheets.keys())
        self.assertEqual(self.spread.sheets["My worksheet"], "A worksheet")

    @patch("gspread.authorize")
    def test_get_records(self, mspread):
        mspread().open().worksheet().get_all_records.return_value = ["hello"]
        self.spread.sheets["A worksheet"] = mspread().open().worksheet()

        response = self.spread.read_records(worksheet="A worksheet")

        self.assertEqual(response, ["hello"])

    @patch("gspread.authorize")
    def test_append_row(self, mspread):
        self.spread.sheets["A worksheet"] = mspread().open().worksheet()
        self.spread.append_row(worksheet="A worksheet", item=["an item"])
        mspread().open().worksheet().append_row.assert_called_once_with(
            values=["an item"]
        )

    @patch("gspread.authorize")
    def test_delete_records(self, mspread):
        self.spread.sheets["A worksheet"] = mspread().open().worksheet()
        self.spread.delete_records(worksheet="A worksheet")
        mspread().open().worksheet().resize.assert_called_once_with(rows=1)
