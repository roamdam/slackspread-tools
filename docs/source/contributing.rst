Contributing
************

All contributions to the package are welcome ! It is likely that the
repository settings on Gitlab are not perfectly tuned for anyone to
contribute, so feel free to contact the package's maintainer if you
encounter any problem.

Since ``slackspread`` is a simple and little package, there are no real rules
when contributing, rather some simple guidelines to keep it clean and
sound.

Coding convention
=================

Code
----

For python's code, we generally follow `PEP8 <https://www.python.org/dev/peps/pep-0008/>`__
convention, for many of our contributors use PyCharm as IDE, which automatically checks
for that convention. As can be seen below, we'd be happy that you add type indications
to a function's signature.

Docstrings
----------

We mostly follow the rules given by ``sphinx`` documentation for docstrings,
about which you can read `here <https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html>`__.

- present tense and imperative mode is preferred for the first sentence, which
  should end with a period
- always use triple quotes for docstrings (``"""Your line."""``) it makes it
easier for next contributors to add new lines to the docstring. 
- for multi-line docstrings, please do not leave a starting blank line
- specify argument types and return with ``:param x:``, ``:type x:``,
  ``:return: something`` and ``:rtype: str`` (for instance)
- try as much as possible to provide at least one usable example
- do not start an argument specification with a cap
- if you write more than one description paragraph, please put all but the first
  *after* the function's arguments specification, but *before examples*

A typical docstring would thus be :

.. code:: python

   def a_wonderful_function(first_arg: int, second_arg: str) -> None:
      """Print first the second, secondly the first."""
       print("A string %s with a number %d" % (second_arg, first_arg))
       return None
   
   def another_function(one_arg: str, prefix: str = 'a prefix : ') -> str:
       """Add a prefix to a string.

       This function is not really useful.

       :param one_arg: the string to be prefixed
       :type one_arg: str
       :param prefix: default ``'a prefix'`` : the prefix to use
       :type prefix: str
       :return: the input string prefixed with the given prefix
       :rtype: str

       Here goes the rest of the description, details, etc.

       :Example:

       >>> another_function('rosso', 'pesto')
       """
       return None

Pull requests
=============

A default template is provided for merge requests. It is not compulsory to use it,
it is only meant to ease reviewing your changes and keeping things simple and
straightforward.

Commit conventions
==================

The more your commits will follow these conventions, the easier it will
be to understand what you did and navigate through the repo's history.

- use present tense and imperative mode for the first line of your commit :
  instead of "Added new function" or "Adds new function", write "Add new function"
- keep the first line less than 80 characters and without period at the end
- feel free to explain with as many lines as needed after the first line, but
  always leave the second one empty
- using emojis to prefix commits is highly appreciated, not only for fun
  but also because it adds another layer of easily understood contextualisation

About emojis, feel free to use whichever you want to, here are the ones that
are the most often used in the package :

- |zap| or |racehorse|            When your changes improve performance
- |pencil2|                       When you modify comments, docstrings or non-python files
- |books|                         When you modify the documentation files (in ``docs/source``) 
- |nut_and_bolt|                  When you change or fix some object mechanism
- |cyclone|                       When you refactor something (more than a little change)
- |bug|                           When you resolve a bug starting from ``develop`` 
- |fire|                          When you resolve a bug starting from ``master``
- |dash|                          When you delete or remove something (in the code or a whole file)
- |art|                           When you improve format and code's appearence (indentation, etc.)
- |arrow_up| or |arrow_down|      When you move files upward or downward in the folder's tree
- |arrow_left| and |arrow_right|  When you move files from a folder to another in the same level of the folder's tree
- |clock1|                        (or any clock) When the changes are temporary or need to be refined
- |construction|                  When the changes are a work in progress and call for subsequent changes
- |white_check_mark|              When you add tests or make broken tests work
- |bus| or |aerial_tramway|       When some work or improvement has been done (in short, when none of the above apply)
- |sparkles|                      When a new functionality has been completed
- |package| and |tada|            Reserved for releases of merge commits
- |green_heart|                   when you did something you're very proud of

.. |zap| replace:: ⚡
.. |racehorse| replace:: 🐎
.. |pencil2| replace:: 📝 
.. |books| replace:: 📚
.. |nut_and_bolt| replace:: 🔩
.. |cyclone| replace:: 🌀
.. |bug| replace:: 🐛
.. |fire| replace:: 🔥
.. |dash| replace:: 💨
.. |art| replace:: 🎨
.. |arrow_up| replace:: ⬆️
.. |arrow_right| replace:: ➡️
.. |arrow_down| replace:: ⬇️
.. |arrow_left| replace:: ⬅️
.. |clock1| replace:: 🕐
.. |construction| replace:: 🚧
.. |white_check_mark| replace:: ✅
.. |bus| replace:: 🚌
.. |aerial_tramway| replace:: 🚡
.. |sparkles| replace:: ✨
.. |package| replace:: 📦
.. |tada| replace:: 🎉
.. |green_heart| replace:: 💚
