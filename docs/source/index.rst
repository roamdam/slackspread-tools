
.. image:: https://gitlab.com/roamdam/slackspread-tools/badges/master/pipeline.svg
   :target: https://gitlab.com/roamdam/slackspread-tools/commits/master
   :alt: Pipeline Status

.. image:: https://codecov.io/gl/roamdam/slackspread-tools/branch/develop/graph/badge.svg
  :target: https://codecov.io/gl/roamdam/slackspread-tools

.. image:: https://readthedocs.org/projects/slack-and-gspread-tools/badge/?version=latest
   :target: https://slack-and-gspread-tools.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status

.. image:: https://badge.fury.io/py/slackspread.svg
   :target: https://badge.fury.io/py/slackspread
   :alt: PyPi status

----------

.. include:: ../../README.rst


.. toctree::
   :maxdepth: 1
   :caption: Modules

   slackbot
   easyspread

.. toctree::
   :maxdepth: 1
   :caption: Other content

   release_notes
   contributing

* :ref:`genindex`
