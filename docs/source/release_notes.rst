Release notes
=============

0.1.2
-----

💨 Clean script content to conform flake8 and black formatting.

0.1.1
-----

🎉 Allow to specify which fields of gspread credentials to replace with environment variables
   by providing ``environ_vars`` at ``Gspread`` instanciation.

0.1.0
-----

🐛 Add missing ``CLIENT_X509_CERT_URL`` to changed environment variables in spreadsheet credentials.

0.0.3
-----

📚 Improve the README, write docstrings and usage documentation.

✅ Improve test coverage and mock every API call during tests so that tests can
be made without network.

0.0.2
-----

🐛 Delete an intempestive print not supposed to be here during execution.

0.0.1
-----

🎉 Initial release !
